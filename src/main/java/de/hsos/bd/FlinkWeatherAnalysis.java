package de.hsos.bd;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple1;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.examples.java.clustering.KMeans;

import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.api.java.operators.IterativeDataSet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import ecmwf.DataServer;

import org.ejml.simple.SimpleMatrix;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.json.JSONObject;
import ucar.ma2.Array;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.*;

public class FlinkWeatherAnalysis {

	final static ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

	// Variables for centroid computation
	static DataSet<KMeans.Centroid> finalCentroids;
    static DataSet<Tuple1<KMeans.Centroid>> finalCentroidsCsv;
    static List<KMeans.Centroid> finalCentroidsList;
    static SimpleMatrix aa;
    static SimpleMatrix ab;

    // Set period of requested data
    static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    static Date start;
    static Date end;
    static int daysToRead = 0;
    static String dateParameter;

    // Set filename and create netcdf file
    static NetcdfFile ncfile = null;
    static String filename = "data.netcdf";
    static String filepath = "data.netcdf";

    // Set are of requested data
    static Double lat_osna = 52.272781;
    static Double lon_osna = 8.048778;

	public static void main(String[] args) throws Exception {
    	try	{
		    if (args.length != 0) {
                filepath = args[0];
            }

            System.out.println("netcdf filepath: " + filepath);

            start = simpleDateFormat.parse("2000-01-01");
            end = simpleDateFormat.parse("2010-12-31");
            daysToRead = Days.daysBetween(new LocalDate(start), new LocalDate(end)).getDays() + 1;
            dateParameter = simpleDateFormat.format(start) + "/to/" + simpleDateFormat.format(end);

            System.out.println("number of days: " + daysToRead);

		    // Open netcdf file and start process method
			ncfile = NetcdfFile.open(filepath);
			System.out.println("\n\nnetcdf file opened");
			process( ncfile, daysToRead, args);
		}
		// If no NetCDF file has been found, download data from ecmwf
        catch (FileNotFoundException fnf) {
    	    // Download data
            retrieveDataFromEcmwf(dateParameter);
            // Open netcdf file and start process method
            ncfile = NetcdfFile.open(filename);
            System.out.println("\n\nnetcdf file opened");
            process( ncfile, daysToRead, args);
        }
        catch (IOException ioe) {
            System.out.println("trying to open " + filename + ", Exception: " + ioe);
        }
		finally {
			if (null != ncfile) try {
				ncfile.close();
			} catch (IOException ioe) {
				System.out.println("trying to close " + filename + ", Exception: " + ioe);
			}
		}
	}

	public static void retrieveDataFromEcmwf(String dateParameter)
    {
        // Server connection to ecmwf
        DataServer server = new DataServer();

        // JSON object to send request parameters
        JSONObject request = new JSONObject();
        try {
            String area = (lat_osna+0.1) + "/" + (lon_osna-0.1) + "/" + (lat_osna-0.1) + "/" + (lon_osna+0.1);

            System.out.println(dateParameter);
            System.out.println(area);
            // Put parameters in request JSON-Object
            request.put("class", "ei");
            request.put("dataset", "interim");
            request.put("date", "2000-01-01/to/2010-12-31");
            request.put("levtype", "sfc");
            request.put("step", "0");
            request.put("time", "00:00:00/06:00:00/12:00:00/18:00:00");
            request.put("type", "an");
            request.put("param", "167.128");
            request.put("stream", "oper");
            request.put("area", "52.372781/7.948778000000001/52.172781/8.148778");
            request.put("grid", "0.05/0.05");
            request.put("target", "data.netcdf");
            request.put("format", "netcdf");

            // Initiate retrieval
            server.retrieve(request);

        } catch (Exception e) {
            System.out.println("Error retrieving Data from ecmwf server, " + e);
        }
    }

    public static void process(NetcdfFile ncfile, int daysToRead, String[] args) {
	    // Set name of variable which will be processed
	    String varName = "t2m";

        try {
            // Read Dimension of data in NetCDF file
            List<Dimension> dim = ncfile.getDimensions();
            // Get Dimensions, size[0] = day, size[1] = latitude, size[2] = longitude
			int[] size = new int[] {1, dim.get(0).getLength(), dim.get(1).getLength()};
            System.out.println(size[0] + " " + size[1] + " " + size[2] + "\n");

            // Find t2m variable
            Variable v = ncfile.findVariable(varName);
            if (null == v) return;

            // Save attributes to convert data from short to double
            Attribute scale_factor = v.findAttribute("scale_factor");
            Attribute add_offset = v.findAttribute("add_offset");

            System.out.println(scale_factor.toString());
            System.out.println(add_offset.toString());
            System.out.println();

            Double temp;
            SimpleMatrix tempOriginal = new SimpleMatrix(daysToRead*4,2);

            // Read temperature data from NetCDF file and decompress
			for (int i = 0; i < daysToRead*4; i++) {
                Array data = v.read(new int[] {i, 0, 0}, size);

                // Get average from all values given for a specific time
				float result = 0;
                for ( int index = 0; index < size[1] * size[2] ; index++ ) {
                    result += data.getDouble(index);
                }
                temp = add_offset.getNumericValue().doubleValue() + result/(size[1]*size[2]) * scale_factor.getNumericValue().doubleValue();
                //System.out.println(temp);
                tempOriginal.set(i, 1, temp);
                tempOriginal.set(i, 0, i+1);
			}
            //System.out.println(matrix);

            // Linear regression necessary, since it computes slightly different values from the given compression
			aa = linreg(tempOriginal, daysToRead);

			// Extract temperature for normalization and time vector (t)
            SimpleMatrix tempNormalize = tempOriginal.extractVector(false, 1);
            SimpleMatrix time = tempOriginal.extractVector(false, 0);

            //matrix.saveToFileCSV("original");

            // Normalize temperature values with linear regression results
            for (int i = 0; i<daysToRead*4; i++){
                tempNormalize.set(i, (tempNormalize.get(i) - ((aa.get(0) * time.get(i)) + aa.get(1))) );
            }

            SimpleMatrix g = periodicRegression(daysToRead, tempNormalize, time);
            System.out.println("Periodic Regression finished");
            DataSet<Tuple2<Integer, KMeans.Point>> clusteredPoints = clustering(g, tempNormalize, daysToRead);
            System.out.println("Clustering finished");

            // Write clustered values and centroids to csv files
            clusteredPoints.writeAsCsv("clusteredPoints.csv", "\n", " ", FileSystem.WriteMode.OVERWRITE);
            finalCentroidsCsv.writeAsCsv("finalCentroids.csv", "\n", " ", FileSystem.WriteMode.OVERWRITE);
            // print() triggers execute() already, no need to explicitely call
            finalCentroids.print();
        } catch (IOException ioe) {
            System.out.println("trying to read " + varName + ", Exception: " + ioe);
        } catch (InvalidRangeException e) {
            System.out.println("invalid Range for " + varName+ ", Exception: " + e);
        } catch (Exception e) {
			System.out.println("Exception in process: " + e);
		}
	}

    public static SimpleMatrix periodicRegression(int daysToRead, SimpleMatrix temp, SimpleMatrix t) {
        try {
            SimpleMatrix R = new SimpleMatrix(daysToRead * 4, 4);
            // Regression indexes go from 0 to n-1, therefore j-1
            for (int j = 0; j < daysToRead * 4; j++) {
                R.set(j, 0, Math.cos(2 * Math.PI / (365 * 4) * (t.get(j)-1)));
                R.set(j, 1, Math.sin(2 * Math.PI / (365 * 4) * (t.get(j)-1)));
                R.set(j, 2, Math.cos(2 * Math.PI / 4 * (t.get(j)-1)));
                R.set(j, 3, Math.sin(2 * Math.PI / 4 * (t.get(j)-1)));
            }
            //System.out.println(R);

            ab = (R.transpose().mult(R)).invert().mult(R.transpose().mult(temp));
            ab.print();
            return R.mult(ab);
        } catch (Exception e) {
        System.out.println("Exception: " + e);
        }
        return null;
    }

    public static DataSet<Tuple2<Integer, KMeans.Point>> clustering(SimpleMatrix g, SimpleMatrix temp, int daysToRead) {
        try {
            // Compute differences between normal and actual temperature values
            SimpleMatrix e = temp.minus(g);
            // Checking input parameters for clustering
            final ParameterTool params = ParameterTool.fromSystemProperties();

            // Set up execution environment
            env.getConfig().setGlobalJobParameters(params); // make parameters available in the web interface

            // Get input data from Matrix e
            System.out.println("Getting Data");
            DataSet<KMeans.Point> points = getPointDataSet(e, daysToRead ,env);

            // Read centroids from csv or default
            System.out.println("Getting Centroids");
            DataSet<KMeans.Centroid> centroids = getCentroidDataSet(env);

            centroids.print();

            System.out.println("Starting iterations");
            // Set number of bulk iterations for KMeans algorithm (new: 30, pre-existing: 5)
            IterativeDataSet<KMeans.Centroid> loop = getLoops(centroids, params);

            DataSet<KMeans.Centroid> newCentroids = points
                    // Compute closest centroid for each point
                    .map(new KMeans.SelectNearestCenter()).withBroadcastSet(loop, "centroids")
                    // Count and sum point coordinates for each centroid
                    .map(new KMeans.CountAppender())
                    .groupBy(0).reduce(new KMeans.CentroidAccumulator())
                    // Compute new centroids from point counts and coordinate sums
                    .map(new KMeans.CentroidAverager());

            // Feed new centroids back into next iteration
            finalCentroids = loop.closeWith(newCentroids);

            // Centroids as list
            finalCentroidsList = finalCentroids.collect();
            // Centroids as Tuples to save them in csv file
            finalCentroidsCsv = finalCentroids.map(new TupleConverter());

            return points
                    // assign points to final clusters
                    .map(new KMeans.SelectNearestCenter()).withBroadcastSet(finalCentroids, "centroids");
        }
        catch (Exception exception) {
            System.out.println(" Exception: " + exception);
            return null;
        }
    }

    public static IterativeDataSet<KMeans.Centroid> getLoops(DataSet<KMeans.Centroid> centroids, ParameterTool params) {
        File file = new File("finalCentroids.csv" );
        IterativeDataSet<KMeans.Centroid> loop;
        if (file.exists()) {
            return loop = centroids.iterate(params.getInt("iterations", 5));
        }
        else {
            return loop = centroids.iterate(params.getInt("iterations", 30));
        }
    }

    // Read centroids from csv, if they exist; else create default centroids
    public static DataSet<KMeans.Centroid> getCentroidDataSet(ExecutionEnvironment env) {
        File file = new File("finalCentroids.csv" );
        DataSet<KMeans.Centroid> newCentroidsCsv;
        if (file.exists()) {
            System.out.println("Loading existing centroids");
            newCentroidsCsv = env.readCsvFile("finalCentroids.csv").fieldDelimiter(" ").pojoType(KMeans.Centroid.class, "id", "x", "y");

            return newCentroidsCsv;
        }
        else {
            System.out.println("Creating default centroids");
            Object[][] CENTROIDS = new Object[][] {
                new Object[]{1, 0., -4.},
                new Object[]{2, 0., -2.},
                new Object[]{3, 0., 0.},
                new Object[]{4, 0., 2.},
                new Object[]{5, 0., 4.}
            };
            System.out.println("Created Objects");
            List<KMeans.Centroid> centroidList = new LinkedList<KMeans.Centroid>();
            for (Object[] centroid : CENTROIDS) {
                centroidList.add(
                    new KMeans.Centroid((Integer) centroid[0], (Double) centroid[1], (Double) centroid[2]));
            }
            System.out.println("Created List");
            return env.fromCollection(centroidList);
        }
    }

    // Convert Matrix e with temperature deviations to List<KMeans.Point> for algorithm
    public static DataSet<KMeans.Point> getPointDataSet(SimpleMatrix e, int daysToRead, ExecutionEnvironment env) {
        List<KMeans.Point> pointList = new LinkedList<KMeans.Point>();
        for (int i = 0; i < daysToRead*4; i++) {
            pointList.add(new KMeans.Point(0., e.get(i)));
        }
        return env.fromCollection(pointList);
    }

    public static SimpleMatrix linreg(SimpleMatrix matrix, int daysToRead) {
	    //SimpleMatrix temp = new SimpleMatrix(daysToRead*4, 1);
	    SimpleMatrix temp = matrix.extractVector(false, 1);
	    SimpleMatrix R = matrix.copy();
	    SimpleMatrix ones = new SimpleMatrix(daysToRead*4, 1);
	    ones.set(1);
	    R.insertIntoThis(0,1, ones);
        //System.out.println(R);

        //SimpleMatrix aa = new SimpleMatrix(2,1);
        SimpleMatrix aa = (R.transpose().mult(R)).invert().mult(R.transpose().mult(temp));
        System.out.println(aa);
        return aa;
    }
}