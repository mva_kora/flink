package de.hsos.bd;

import org.apache.flink.api.java.tuple.Tuple1;
import org.apache.flink.streaming.api.datastream.*;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import ucar.nc2.NetcdfFile;

import java.io.FileNotFoundException;
import java.io.IOException;

import static de.hsos.bd.FlinkWeatherAnalysis.*;
import static de.hsos.bd.FlinkWeatherAnalysis.daysToRead;

public class RealTimeDataInput {

    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        try {
            if (args.length != 0) {
                filepath = args[0];
            }

            System.out.println("netcdf filepath: " + filepath);

            start = simpleDateFormat.parse("2000-01-01");
            end = simpleDateFormat.parse("2010-12-31");
            daysToRead = Days.daysBetween(new LocalDate(start), new LocalDate(end)).getDays() + 1;
            dateParameter = simpleDateFormat.format(start) + "/to/" + simpleDateFormat.format(end);

            System.out.println("number of days: " + daysToRead);

            //Open netcdf file and start process method
            ncfile = NetcdfFile.open(filepath);
            System.out.println("\n\nnetcdf file opened");
            process( ncfile, daysToRead, args);
        }
        // If no NetCDF file has been found, download data from ecmwf
        catch (FileNotFoundException fnf) {
            // Download data
            retrieveDataFromEcmwf(dateParameter);
            //Open netcdf file and start process method
            ncfile = NetcdfFile.open(filename);
            System.out.println("\n\nnetcdf file opened");
            process( ncfile, daysToRead, args);
        }
        catch (IOException ioe) {
            System.out.println("trying to open " + filename + ", Exception: " + ioe);
        }
        finally {
            if (null != ncfile) try {
                ncfile.close();
            } catch (IOException ioe) {
                System.out.println("trying to close " + filename + ", Exception: " + ioe);
            }
            try {
                System.out.println("Processing finished");
                // Add custom source, execute map for each element in the stream (here: just 1 current temperature object)
                // countWindow(1): execute map(new GetCurrentTemperatureFeature() for each collect()
                DataStream<Tuple1<String>> stream = env.addSource(new OpenWeatherMapBoundary(lon_osna, lat_osna))
                        .map(new ComputeClusters())
                        .keyBy(0)
                        .countWindow(1)
                        .min(0)
                        .map(new GetCurrentTemperatureFeature());

                stream.print();

                env.execute("FlinkRealTimeWeatherAnalysis");
            }
            catch(Exception e) {
                System.out.println("Exception in RealTimeDataInput finally clause: " + e + " message: " + e.getMessage());
            }
        }
    }
}