package de.hsos.bd;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple1;

public class GetCurrentTemperatureFeature implements MapFunction<Tuple1<Integer>, Tuple1<String>> {
    @Override
    public Tuple1<String> map (Tuple1<Integer> cluster) {
        //System.out.println("Map GetCurrentTemperatureFeature for: " + cluster.f0);

        if ( cluster.f0 == 1 ) {
            return new Tuple1<String>("Sehr kalt");
        }
        else if ( cluster.f0 == 2 ) {
            return new Tuple1<String>("Kalt");
        }
        else if ( cluster.f0 == 3 ) {
            return new Tuple1<String>("Normal");
        }
        else if ( cluster.f0 == 4 ) {
            return new Tuple1<String>("Warm");
        }
        else if ( cluster.f0 == 5 ) {
            return new Tuple1<String>("Sehr warm");
        }
        else {
            return new Tuple1<String>("Unbekannt");
        }
    }
}

