package de.hsos.bd;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple1;
import org.apache.flink.api.java.tuple.Tuple2;
import org.joda.time.DateTime;
import org.joda.time.Days;
import java.util.List;

import static de.hsos.bd.FlinkWeatherAnalysis.*;

public class ComputeClusters implements MapFunction<Tuple1<Double>, Tuple1<Integer>> {
    static Double differenceFromNormal;
    @Override
    public Tuple1<Integer> map (Tuple1<Double> currentTemperature) {
        try {
            System.out.println("Map ComputeClusters for: " + currentTemperature.f0.toString());

            Double temperature = currentTemperature.f0;

            // We need to get the current day in index form since the "start" date
            // Therefore we need to get the elapsed days*4 (4 time values per day)
            Double currentDayIndex = new Double(Days.daysBetween(new DateTime(start), new DateTime()).getDays() *4);

            // We also need to add the approximate index of the current day
            DateTime currentTime = new DateTime();
            int hours = currentTime.getHourOfDay();

            currentDayIndex += hours / 24.0 * 4.0;

            // Normalize current temperature value
            temperature = temperature - (aa.get(0) * currentDayIndex + aa.get(1));

            // Compute the "normal" value from the periodic regression
            Double periodRegTemp = ab.get(0) * Math.cos(2 * Math.PI / (365 * 4) * currentDayIndex)
                    + ab.get(1) * Math.sin(2 * Math.PI / (365 * 4) * currentDayIndex)
                    + ab.get(2) * Math.cos(2 * Math.PI / 4 * currentDayIndex)
                    + ab.get(3) * Math.sin(2 * Math.PI / 4 * currentDayIndex);

            // Get the difference between the current and normal value
            differenceFromNormal = temperature - periodRegTemp;

            System.out.println("daysToRead*4 value for current day + additionalHours " + currentDayIndex + ", today value: " + hours / 24.0 * 4.0);
            System.out.println("daysToRead " + daysToRead*4);
            System.out.println("periodRegTemp = " + periodRegTemp);
            System.out.println("linRegTemp = " + temperature);
            System.out.println("differenceFromNormal = " + differenceFromNormal);

            // Compute distance from all centroids with map function
            List<Tuple2<Double, Integer>> clusterDistances = finalCentroids.map(new GetClusterDistance()).collect();

            // Get the minimum distance and extract relevant cluster using minIndex
            Double min = Double.MAX_VALUE;
            int minIndex = 0;
            for(int i=0; i<clusterDistances.size(); i++){
                if(clusterDistances.get(i).f0 < min){
                    minIndex = i;
                    min = clusterDistances.get(i).f0;
                }
            }
            //System.out.println("MinListValue = " + clusterDistances.get(minIndex));

            // Return cluster integer value using minIndex
            return new Tuple1<Integer>(clusterDistances.get(minIndex).f1); //in.f0.toString()
            //System.out.println(finalCentroidsList);
        } catch (Exception e) {
            System.out.println("Exception in ComputeClusters(): " + e + ", message: " + e.getMessage());
            return new Tuple1<Integer>(0);
        }
    }
}


