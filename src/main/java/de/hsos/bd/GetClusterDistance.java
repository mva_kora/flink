package de.hsos.bd;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.examples.java.clustering.KMeans;

public class GetClusterDistance implements MapFunction<KMeans.Centroid, Tuple2<Double, Integer>> {
    @Override
    public Tuple2<Double, Integer> map (KMeans.Centroid centroid) {

        System.out.println("Clusterdistance: " + centroid.id + " = " + Math.abs(centroid.y - ComputeClusters.differenceFromNormal));
        return new Tuple2<Double, Integer>(Math.abs(centroid.y - ComputeClusters.differenceFromNormal), centroid.id); //in.f0.toString()
    }
}
