package de.hsos.bd;

import org.apache.flink.api.java.tuple.Tuple1;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class OpenWeatherMapBoundary implements SourceFunction<Tuple1<Double>> {
    private volatile boolean isRunning = true;
    private final String basicOpenWeatherMapURI = "http://api.openweathermap.org/data/2.5/weather?";
    private final String openWeatherMapId = "6f42fc67e2e2de31174b4f34eb6216d1";
    private Double longitude = 52.272781; // Default Osnabrueck
    private Double latitude = 8.048778;

    public OpenWeatherMapBoundary(Double longitude, Double latitude){
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public Tuple1<Double> startRequestForTemperatureData(Double longitude, Double latitude) throws MalformedURLException,
            IOException, JSONException{
        try {
            //System.out.println("startRequestForTemperatureData");
            Double currentTemperature = 273.15; //in Kelvin
            String result = "";
            String uri = this.createRequestURI(longitude, latitude);

            URL uriRequest = new URL(uri);
            HttpURLConnection httpURLConnection = (HttpURLConnection) uriRequest.openConnection();

            System.out.println("New request at time: " + new java.util.Date());
            if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {

                InputStreamReader inputStreamReader = new InputStreamReader(httpURLConnection.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String line = null;

                while ((line = bufferedReader.readLine()) != null) {
                    result += line;
                }

                bufferedReader.close();
                // Extract current temperature from OpenWeatherMap result
                currentTemperature = parseResult(result);

            } else {
                System.out.println("Error: No connection available.");
                throw new IOException();
            }

            // Return currentTemperature as Tuple1<Double> for optimal further usage
            return new Tuple1<Double>(currentTemperature);
        }
        catch(Exception e)
        {
            System.out.println("Exception in startRequestForTemperatureData: " + e);
            return new Tuple1<Double>(0.0);
        }
    }

    // run method for custom addSource()
    @Override
    public void run(SourceContext<Tuple1<Double>> ctx) throws Exception {
        System.out.println("Running custom source");
        // We need to keep the thread running!
        while (isRunning)
        {
            ctx.collect(this.startRequestForTemperatureData(this.longitude, this.latitude));
            // Fire new request every 5000 milliseconds
            Thread.sleep(5000);
        }
    }

    @Override
    public void cancel() {
        isRunning = false;
    }

    // Request URI for OpenWeatherMap
    private String createRequestURI(Double longitude, Double latitude){
        //System.out.println("CreateRequestURI");
        String uri = String.format("%slat=%f&lon=%f&APPID=%s", basicOpenWeatherMapURI,
                latitude, longitude, openWeatherMapId);
        return uri;
    }

    // Extract current temperature from result json
    private Double parseResult(String json) throws JSONException{
        //System.out.println("ParseResult");
        JSONObject jsonObject = new JSONObject(json);
        Double temp = 273.15;

        JSONObject JSONObject_main = jsonObject.getJSONObject("main");
        temp = JSONObject_main.getDouble("temp");
        System.out.println("ParseResult temp: " + temp);
        return temp;
    }
}
