package de.hsos.bd;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple1;
import org.apache.flink.examples.java.clustering.KMeans;

public class TupleConverter implements MapFunction<KMeans.Centroid, Tuple1< KMeans.Centroid>> {
    @Override
    public Tuple1< KMeans.Centroid> map (KMeans.Centroid in) {
        return new Tuple1<KMeans.Centroid>( in);
    }
}
